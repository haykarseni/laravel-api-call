<?php
namespace App\Services;
use Illuminate\Support\Facades\Http;

class OmdbapiService {

    const BASE_URL = 'http://www.omdbapi.com';

    static public function searchByTitle($title = null, $year = null) {
        $response = null;
        try {

            $response = Http::get(self::BASE_URL . '?apikey=' . config('app.omdb_api_key') . '&i=tt3896198&t=' . $title . ($year ? '&y=' . $year : ''));

            $response->throw();
            $data = $response->json();

            return [$data, $response->ok() ? null : [], $response->status()];
        } catch (\Exception $e) {
            return [null, $e->getMessage(), $response ? $response->status() : null];
        }
    }
}
