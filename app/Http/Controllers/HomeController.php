<?php

namespace App\Http\Controllers;

use App\Services\OmdbapiService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request) {
        $title = $request->input('title');
        $year = $request->input('year');
        $data = null;
        if($title) {
            [$data, $error] = OmdbapiService::searchByTitle($title, $year);
        }

        return view('search', ['json' => $data, 'title' => $title, 'year' => $year]);
    }
}
