<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f0f0f0;
                margin: 0;
                padding: 0;
            }
            #response {
                margin-top: 25px;
            }
            .container {
                max-width: 800px;
                margin: 0 auto;
                padding: 20px;
                background-color: #fff;
                box-shadow: 0 2px 4px rgba(0,0,0,.1);
            }
            h1 {
                font-size: 32px;
                margin-bottom: 20px;
            }
            p {
                font-size: 18px;
                line-height: 1.5;
                margin-bottom: 10px;
            }
            .success {
                color: green;
                font-weight: bold;
            }
            .error {
                color: red;
                font-weight: bold;
            }
        </style>
    </head>
    <body>

    <div class="container">
        <h1>OMDB API request</h1>
        <form method="get">
            <label for="title">Title:</label>
            <input type="text" value="{{ $title }}" id="title" name="title">
            <label for="year">Year:</label>
            <input type="text" value="{{ $year }}" id="year" name="year">
            <button type="submit">Search</button>
        </form>
        <pre id="response" style="white-space: pre-wrap;"></pre>
    </div>
    </body>
</html>
<script>
    const str = JSON.stringify(JSON.parse("{{$json ? json_encode($json) : ''}}".replaceAll('&quot;', '\"')), null, 2);
    document.getElementById('response').textContent = str;
</script>
